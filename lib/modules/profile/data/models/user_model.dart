
class UserModel {
  final String userName;
  final String email;
  final String password;
  final num coins;
  final List<QuizUserModel> quizzes;


  UserModel({
        required this.userName,
        required this.email,
        required this.password,
        required this.coins,
        required this.quizzes
      });

  factory UserModel.fromJson(Map<String,dynamic> json)=>
      UserModel(
          userName: json['user_name'],
          email: json['email'],
          password: json['password'],
          coins: json['coins'] ?? 0,
          quizzes: ((json['quizzes'] ?? []) as List).map((e) => QuizUserModel.fromJson(e)).toList(),
      );


}

class QuizUserModel {
  final String name;
  final num percentage;

  QuizUserModel({required this.name,required this.percentage});

  factory QuizUserModel.fromJson(Map<String,dynamic> json)=>
      QuizUserModel(
        name:json['name'],
        percentage: json['percentage']
      );

}