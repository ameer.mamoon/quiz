
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quizzy/core/storage/storage_handler.dart';
import 'package:quizzy/modules/quiz/data/models/section_model.dart';

import '../models/user_model.dart';

class ProfileDataSource{

  static final _fb = FirebaseFirestore.instance;

  static Future<UserModel> getProfile()async{
    var collection = _fb.collection("users");
    var result = await collection.where('email',isEqualTo: StorageHandler().email).get();
    return UserModel.fromJson(result.docs.first.data());
  }




}