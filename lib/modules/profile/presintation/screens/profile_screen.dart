import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizzy/core/core_components/app_logo.dart';
import 'package:quizzy/modules/quiz/presintation/controller/sections_cubit.dart';
import 'package:quizzy/modules/quiz/presintation/controller/sections_cubit.dart';
import 'package:quizzy/modules/quiz/presintation/screens/section_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/consts/assets_const.dart';
import '../../../../core/core_components/status_component.dart';
import '../controller/profile_cubit.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  static const name = '/profile';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppLogo(),
      ),
      body: BlocBuilder<ProfileCubit, ProfileState>(
        builder: (context, state) {
          return StatusComponent(
              status: state.dataState.status,
              onSuccess: (context)
              => SingleChildScrollView(
                child: Container(
                    width: double.infinity,
                    padding: EdgeInsets.all(5.w),
                    child: Column(
                      children: [
                        Container(
                          width: 35.w,
                          height: 35.w,
                          margin: EdgeInsets.only(bottom: 1.w),
                          decoration: BoxDecoration(
                            border: Border.all(),
                            shape: BoxShape.circle,
                            color: Colors.grey[200]
                          ),
                          alignment: Alignment.center,
                          child: Icon(Icons.person_outline,size: 20.w,),
                        ),
                        Text(
                            'Profile',
                            style: TextStyle(
                              fontSize: 24.sp,
                              fontWeight: FontWeight.bold
                            ),
                        ),
                        SizedBox(
                          height: 5.w,
                        ),
                        _rowData('User name',state.dataState.data!.userName),
                        _rowData('Email',state.dataState.data!.email),
                        _rowData('Password',state.dataState.data!.password),
                        SizedBox(
                          height: 5.w,
                        ),
                        DefaultTextStyle(
                          style: TextStyle(
                              fontSize: 16.sp,
                              color: const Color(0xFF4257B2),
                              fontWeight: FontWeight.bold
                          ),
                          child: const Row(
                            children: [
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.center,
                                      child: Text('Quizzes passed')
                                  )
                              ),
                              Expanded(
                                  child: Align(
                                      alignment: Alignment.center,
                                      child: Text('Total Coin')
                                  )
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5.w,
                        ),
                        DefaultTextStyle(
                          style: TextStyle(
                            fontSize: 28.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.bold
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Container(
                                    width: 35.w,
                                    height: 35.w,
                                    margin: EdgeInsets.only(bottom: 1.w),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          width: 2.5.w,
                                          color: Color(0xFF0EF9B9)
                                        ),
                                        shape: BoxShape.circle,
                                    ),
                                    alignment: Alignment.center,
                                    child: Text(
                                      state.dataState.data!.quizzes.length.toString(),
                                    ),
                                  )
                              ),
                              Expanded(
                                  child: Container(
                                    width: 35.w,
                                    height: 35.w,
                                    margin: EdgeInsets.only(bottom: 1.w),
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                          width: 2.5.w,
                                          color: Color(0xFFFB9206)
                                        ),
                                        shape: BoxShape.circle,
                                    ),
                                    alignment: Alignment.center,
                                    child: Text(
                                      state.dataState.data!.coins.toString(),
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 5.w,
                        ),
                        GridView.builder(
                            shrinkWrap: true,
                            physics: const PageScrollPhysics(),
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 2.5.w,
                              crossAxisSpacing: 2.5.w
                            ),
                          itemCount: state.dataState.data!.quizzes.length,
                          itemBuilder: (context,i) => Container(
                            decoration: BoxDecoration(
                              border: Border.all()
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                    state.dataState.data!.quizzes[i].name,
                                    style: TextStyle(
                                      color: Colors.red[600],
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20.sp
                                    ),
                                ),
                                Stack(
                                  children: [
                                    Center(
                                      child: SizedBox(
                                        width:20.w,
                                        height:20.w,
                                        child: const CircularProgressIndicator(
                                          strokeWidth: 8,
                                          color: Colors.yellow,
                                          value: 1
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: SizedBox(
                                        width:20.w,
                                        height:20.w,
                                        child: CircularProgressIndicator(
                                          value: state.dataState.data!.quizzes[i].percentage/100,
                                        ),
                                      ),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: EdgeInsets.only(top: 7.w),
                                        child: Text(
                                            '${state.dataState.data!.quizzes[i].percentage} %',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 16.sp
                                            ),
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                ),
              ),
              onError: (context)=>Center(child: Text(state.dataState.message)),
          );
        },
      ),
    );
  }

  Widget _rowData(String title, String value) =>
      Container(
        width: double.infinity,
        height: 15.w,
        decoration: BoxDecoration(
          border: Border.all(),
          color: Colors.grey[200],
        ),
        padding: EdgeInsets.symmetric(horizontal: 5.w),
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            const VerticalDivider(color: Colors.black,),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  value,
                  style: TextStyle(
                      fontSize: 20.sp,
                      color: Colors.red[600],
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
            )
          ],
        ),
      );
}
