import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizzy/modules/profile/data/models/user_model.dart';

import '../../../../core/data_state/data_state.dart';
import '../../../../core/handler/handler.dart';
import '../../data/data_source/profile_data_source.dart';

part 'profile_state.dart';

class ProfileCubit extends Cubit<ProfileState> {
  ProfileCubit() : super(const ProfileInitialState());

  void load()async{
    emit(const ProfileLoadingState());
    var result = await handle<UserModel>(() => ProfileDataSource.getProfile());
    emit(ProfileResultState(result));
  }
}
