part of 'profile_cubit.dart';


@immutable
abstract class ProfileState {
  final DataState<UserModel> dataState;

  const ProfileState(this.dataState);
}

class ProfileInitialState extends ProfileState {
  const ProfileInitialState():super(const DataState());
}

class ProfileLoadingState extends ProfileState {
  const ProfileLoadingState():super(const DataState(status: DataStatus.loading));
}

class ProfileResultState extends ProfileState {
  const ProfileResultState(super.dataState);
}
