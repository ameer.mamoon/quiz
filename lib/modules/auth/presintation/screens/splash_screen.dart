import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizzy/core/storage/storage_handler.dart';
import 'package:quizzy/core/core_components/app_logo.dart';
import 'package:quizzy/modules/auth/presintation/screens/login_screen.dart';

import '../../../quiz/presintation/screens/home_screen.dart';
import '../controller/splash/splash_cubit.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  static const name = '/';


  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashCubit,SplashState>(
      listener: (context,state){
        if(state is SplashDoneState) {
          if(StorageHandler().hasEmail){
            Navigator.pushReplacementNamed(
              context,
              HomeScreen.name,
            );
          }
            else{
            Navigator.pushReplacementNamed(
              context,
              LoginScreen.name,
            );
          }
        }
      },
      child: const Scaffold(
        body: Center(
            child: AppLogo()
        ),
      ),
    );
  }
}
