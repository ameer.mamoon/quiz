import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:quizzy/core/consts/assets_const.dart';
import 'package:quizzy/core/core_components/app_text_from_field.dart';
import 'package:quizzy/core/core_components/app_logo.dart';
import 'package:quizzy/modules/auth/presintation/screens/register_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/core_components/popups.dart';
import '../../../../core/data_state/data_state.dart';
import '../../../quiz/presintation/screens/home_screen.dart';
import '../components/auth_button.dart';
import '../controller/login/login_cubit.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  static const name = '/login';

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();
  final GlobalKey<FormState> _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        switch (state.dataState.status) {
          case DataStatus.error:
            Navigator.pop(context);
            showSnackBar(context, state.dataState.message);
            break;
          case DataStatus.success:
            Navigator.pushNamedAndRemoveUntil(
                context, HomeScreen.name, (_) => false);
            break;
          case DataStatus.init:
            break;
          case DataStatus.loading:
            break;
        }
      },
      child: Scaffold(
        appBar: AppBar(
          leading: const AppLogo(),
        ),
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(5.w),
            child: Form(
              key: _key,
              child: Column(
                children: [
                  SizedBox(
                    height: 10.w,
                    child: const FittedBox(
                      child: Text(
                        'Login',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 60.w,
                    width: 60.w,
                    child: Image.asset(
                      ImagesConst.login,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                  AppTextFormField(
                      controller: emailController,
                      icon: Icon(Icons.email_outlined),
                      hint: 'email',
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'required';
                      }
                  ),
                  AppTextFormField(
                      controller: passController,
                      icon: Icon(Icons.lock_outline),
                      hint: 'password',
                      validator: (val){
                        if(val == null || val.isEmpty)
                          return 'required';
                      }
                  ),
                  AuthButton(
                    label: 'Sign In',
                    onTap: (){
                      if(!_key.currentState!.validate())
                        return;
                      showLoader(context);
                      context.read<LoginCubit>().login(
                          email: emailController.text,
                          password: passController.text
                      );
                    },
                  ),
                  SizedBox(
                    height: 5.w,
                  ),
                  DefaultTextStyle(
                    style: TextStyle(fontSize: 14.sp, color: Colors.black),
                    child: SizedBox(
                      width: double.infinity,
                      child: Wrap(
                        children: [
                          const Text('if you don\'t have an account '),
                          InkWell(
                            onTap: () {
                              Navigator.pushReplacementNamed(
                                  context, RegisterScreen.name);
                            },
                            child: const Text(
                              'tap to register',
                              style: TextStyle(color: Colors.indigo),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
