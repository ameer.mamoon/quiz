import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class AuthButton extends StatelessWidget {

  final String label;
  final void Function()? onTap;


  const AuthButton({Key? key, required this.label, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(5.w),
        width: double.infinity,
        height: 15.w,
        color: const Color(0xFFFF5454),
        alignment: Alignment.center,
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.sp,
            color: Colors.white
          ),
        ),
      ),
    );
  }
}
