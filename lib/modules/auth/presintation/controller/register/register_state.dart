part of 'register_cubit.dart';

@immutable
abstract class RegisterState {
  final DataState<bool> dataState;

  const RegisterState(this.dataState);
}

class RegisterInitialState extends RegisterState {
  const RegisterInitialState():super(const DataState());
}

class RegisterLoadingState extends RegisterState {
  const RegisterLoadingState():super(const DataState(status: DataStatus.loading));
}

class RegisterResultState extends RegisterState {
  const RegisterResultState(super.dataState);
}
