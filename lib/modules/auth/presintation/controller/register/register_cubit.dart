import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizzy/core/data_state/data_state.dart';
import 'package:quizzy/core/handler/handler.dart';
import 'package:quizzy/modules/auth/data/data_source/auth_data_source.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(const RegisterInitialState());

  void register({
    required String username,
    required String email,
    required String password,})async{
    emit(const RegisterLoadingState());
    var result = await handle<bool>(() => AuthDataSource.register(username: username, email: email, password: password));
    emit(RegisterResultState(result));
  }
}
