part of 'splash_cubit.dart';

@immutable
abstract class SplashState {}

class SplashInitialState extends SplashState {}

class SplashDoneState extends SplashState {}
