import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  SplashCubit() : super(SplashInitialState());

  void splash()async{
    await Future.delayed(const Duration(seconds: 1));
    emit(SplashDoneState());
  }
}
