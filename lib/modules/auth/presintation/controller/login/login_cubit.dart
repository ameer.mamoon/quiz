import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../core/data_state/data_state.dart';
import '../../../../../core/handler/handler.dart';
import '../../../data/data_source/auth_data_source.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  LoginCubit() : super(const LoginInitialState());

  void login({
    required String email,
    required String password,})async{
    emit(const LoginLoadingState());
    var result = await handle<bool>(() => AuthDataSource.login(email: email, password: password));
    emit(LoginResultState(result));
  }
}
