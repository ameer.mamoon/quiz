part of 'login_cubit.dart';


@immutable
abstract class LoginState {
  final DataState<bool> dataState;

  const LoginState(this.dataState);
}

class LoginInitialState extends LoginState {
  const LoginInitialState():super(const DataState());
}

class LoginLoadingState extends LoginState {
  const LoginLoadingState():super(const DataState(status: DataStatus.loading));
}

class LoginResultState extends LoginState {
  const LoginResultState(super.dataState);
}
