
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quizzy/core/storage/storage_handler.dart';

class AuthDataSource{

  static final _fb = FirebaseFirestore.instance;

  static Future<bool> register({
    required String username,
    required String email,
    required String password,})async{
    var collection = _fb.collection("users");
    var users = await collection.where('user_name',isEqualTo: username).
                              where('email',isEqualTo: email).get();
    if(users.docs.isNotEmpty) {
      throw Exception('user name or email is used');
    }
    await collection.add({
      'user_name':username,
      'email':email,
      'password':password,
      'coins':0,
      'quizzes':[],
    });
    await StorageHandler().setEmail(email);
    return true;
  }

  static Future<bool> login({
    required String email,
    required String password,})async{
    var collection = _fb.collection("users");
    var users= await collection.where('password',isEqualTo: password).
    where('email',isEqualTo: email).get();
    if(users.docs.isEmpty) {
      throw Exception('password or email is wrong');
    }
    await StorageHandler().setEmail(email);
    return true;
  }


}