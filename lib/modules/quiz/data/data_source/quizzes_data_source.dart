
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:quizzy/core/storage/storage_handler.dart';
import 'package:quizzy/modules/quiz/data/models/section_model.dart';

class QuizzesDataSource{

  static final _fb = FirebaseFirestore.instance;

  static Future<List<SectionModel>> getSections()async{
    var collection = _fb.collection("secction");
    var result = await collection.get();
    return result.docs.map((e) => SectionModel.fromJson(e.data())).toList();
  }

  static Future<bool> submitQuiz(QuizModel quiz,int correct)async{
    var collection = _fb.collection("users");
    var user = await collection.where(
        'email',isEqualTo: StorageHandler().email
    ).get();
    var userMap = user.docs.first.data();
    List quizzes = userMap['quizzes'] ?? [];

    if(quizzes.where((element) => element['name'] == quiz.name).isNotEmpty){
      quizzes.removeWhere((element) => element['name'] == quiz.name);
      quizzes.add({
        'name':quiz.name,
        'percentage' : ((correct/quiz.questions.length) * 100).toInt()
      });
      userMap['quizzes'] = quizzes;
      await collection.doc(user.docs.first.id).update(userMap);
      return true;
    }
    quizzes.add({
      'name':quiz.name,
      'percentage' : ((correct/quiz.questions.length) * 100).toInt()
    });
    userMap['quizzes'] = quizzes;
    userMap['coins'] = correct + (userMap['coins'] ?? 0);
    await collection.doc(user.docs.first.id).update(userMap);
    return true;
  }



}