
class SectionModel {
  final String name;
  final List<QuizModel> quizzes;

  SectionModel({
    required this.name,
    required this.quizzes
  });

  factory SectionModel.fromJson(Map<String,dynamic> json)=>
      SectionModel(
        name: json['name'],
        quizzes: (json['quizzes'] as List).map((e) => QuizModel.fromJson(e)).toList(),
      );

  Map<String,dynamic> get json => {
    'name':name,
    'quizzes':quizzes.map((e) => e.json).toList()
  };
}

class QuizModel {
  final String name;
  final List<QuestionModel> questions;

  QuizModel({
    required this.name,required this.questions
  });

  factory QuizModel.fromJson(Map<String,dynamic> json)=>
      QuizModel(
        name: json['name'],
        questions: (json['questions'] as List).map((e) => QuestionModel.fromJson(e)).toList(),
      );

  Map<String,dynamic> get json => {
    'name':name,
    'questions':questions.map((e) => e.json).toList()
  };
}

class QuestionModel {
  final String question;
  final String ans;
  final List<String> options;

  QuestionModel({
    required this.question,
    required this.ans,
    required this.options
  });

  factory QuestionModel.fromJson(Map<String,dynamic> json)=>
      QuestionModel(
        question: json['question'],
        ans: json['ans'],
        options: (json['options'] as List).map((e) => e.toString()).toList(),
      );
  Map<String,dynamic> get json => {
    'question':question,
    'ans':ans,
    'options':options
  };
}