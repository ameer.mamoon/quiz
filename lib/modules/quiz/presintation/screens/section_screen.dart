import 'package:flutter/material.dart';
import 'package:quizzy/modules/quiz/data/models/section_model.dart';
import 'package:quizzy/modules/quiz/presintation/screens/quiz_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/consts/assets_const.dart';
import '../../../../core/core_components/app_logo.dart';

class SectionsScreen extends StatelessWidget {
  final SectionModel model;
  const SectionsScreen(this.model,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppLogo(),
        title: Text(model.name),
      ),
      body: Padding(
        padding: EdgeInsets.all(2.5.w),
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 2.5.w,
            crossAxisSpacing: 2.5.w
          ),
          itemCount: model.quizzes.length,
          itemBuilder: (context,i) => GestureDetector(
            onTap: ()=> Navigator.push(context,
              MaterialPageRoute(builder: (context) => QuizScreen(model.quizzes[i])),
            ),
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.blue[600]!
                )
              ),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      '#${i+1}',
                      style: TextStyle(
                        color: Color(0xFFFF5353),
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w900
                      ),
                    ),
                  ),
                  Image.asset(ImagesConst.quiz),
                  Text(model.quizzes[i].name),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
