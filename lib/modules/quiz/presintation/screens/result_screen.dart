import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizzy/modules/quiz/presintation/controller/submit/submit_quiz_cubit.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/core_components/app_logo.dart';
import '../../../../core/core_components/popups.dart';
import '../../../../core/data_state/data_state.dart';
import '../../data/models/section_model.dart';
import 'home_screen.dart';

class ResultScreen extends StatelessWidget {
  final QuizModel model;
  final List<String> answers;

  const ResultScreen({Key? key, required this.model, required this.answers})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SubmitQuizCubit, SubmitQuizState>(
      listener: (context, state) {
        switch(state.dataState.status){
          case DataStatus.error:
            Navigator.pop(context);
            showSnackBar(context, state.dataState.message);
            break;
          case DataStatus.success:
            Navigator.pushNamedAndRemoveUntil(context, HomeScreen.name,(_)=>false);
            break;
          case DataStatus.init:
            break;
          case DataStatus.loading:
            break;
      }
      },
      child: Scaffold(
        appBar: AppBar(
          leading: const AppLogo(),
        ),
        body: SingleChildScrollView(
          child: Container(
            width: double.infinity,
            padding: EdgeInsets.all(5.w),
            child: Column(
              children: [
                Text(
                    'End of Quiz',
                    style: TextStyle(
                      fontSize: 24.sp,
                      fontWeight: FontWeight.bold,
                    )
                ),
                SizedBox(
                  height: 10.w,
                ),
                Icon(Icons.star, color: Colors.amber, size: 35.w,),
                SizedBox(
                  height: 10.w,
                ),
                _rowData('Total Questions', model.questions.length),
                _rowData('correct answers', correct),
                _rowData('wrong answers', wrong),
                _rowData('your coins', correct),
                SizedBox(
                  height: 10.w,
                ),
                GestureDetector(
                  onTap: (){
                    showLoader(context);
                    context.read<SubmitQuizCubit>().submit(model, correct);
                  },
                  child: Container(
                    width: 80.w,
                    height: 15.w,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.w),
                      color: Color(0xFFFF5353),
                    ),
                    alignment: Alignment.center,
                    margin: EdgeInsets.all(5.w),
                    child: Text(
                      'Submit',
                      style: TextStyle(
                          fontSize: 20.sp,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _rowData(String title, int value) =>
      Container(
        width: 80.w,
        height: 15.w,
        decoration: BoxDecoration(
          border: Border.all(),
          color: Colors.grey[200],
        ),
        padding: EdgeInsets.symmetric(horizontal: 5.w),
        child: Row(
          children: [
            Expanded(
              flex: 5,
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 20.sp,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            const VerticalDivider(color: Colors.black,),
            Expanded(
              child: Align(
                alignment: Alignment.centerRight,
                child: Text(
                  value.toString(),
                  style: TextStyle(
                      fontSize: 20.sp,
                      color: Colors.blue[600],
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
            )
          ],
        ),
      );

  int get correct {
    int count = 0;
    for (int i = 0; i < answers.length; i ++) {
      if (answers[i] == model.questions[i].ans)
        count ++;
    }
    return count;
  }

  int get wrong => answers.length - correct;

}
