import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizzy/modules/quiz/presintation/screens/result_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/core_components/app_logo.dart';
import '../../data/models/section_model.dart';
import '../controller/submit/submit_quiz_cubit.dart';

class QuizScreen extends StatelessWidget {
  final QuizModel model;
  final List<String> answers = [];

  QuizScreen(this.model, {Key? key}) : super(key: key);

  final PageController pageController = PageController(initialPage: 0);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppLogo(),
        title: Text(model.name),
      ),
      body: PageView.builder(
          physics: const NeverScrollableScrollPhysics(),
          controller: pageController,
          itemCount: model.questions.length,
          itemBuilder: (context, i) =>
              Container(
                width: double.infinity,
                height: double.infinity,
                padding: EdgeInsets.all(5.w),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.lightbulb,
                          size: 10.w,
                        ),
                        SizedBox(
                          width: 2.5.w,
                        ),
                        Text(
                            'Question ${i + 1} of ${model.questions.length}',
                            style: TextStyle(
                                fontSize: 24.sp,
                                fontWeight: FontWeight.bold,
                                color: Color(0xFF3CCFCF)
                            )
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.w,
                    ),
                    Text(
                      model.questions[i].question,
                      style: TextStyle(
                          fontSize: 22.sp,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(
                      height: 5.w,
                    ),
                    Expanded(
                      child: ListView.builder(
                          itemCount: model.questions[i].options.length,
                          itemBuilder: (context, j) =>
                              GestureDetector(
                                onTap: () =>
                                    _onSelect(model.questions[i].options[j],
                                        context),
                                child: Container(
                                  height: 20.w,
                                  margin: EdgeInsets.all(2.5.w),
                                  decoration: BoxDecoration(
                                      color: Color(0xFFFF5353),
                                      borderRadius: BorderRadius.circular(8.w)
                                  ),
                                  alignment: Alignment.center,
                                  child: Text(
                                    model.questions[i].options[j],
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.sp,
                                        fontWeight: FontWeight.bold
                                    ),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ),
                              )
                      ),
                    )
                  ],
                ),
              )
      ),
    );
  }

  void _onSelect(String ans, BuildContext context) {
    answers.add(ans);
    if (answers.length == model.questions.length) {
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) =>
          BlocProvider(
            create: (context) => SubmitQuizCubit(),
            child: ResultScreen(model: model, answers: answers.toList(),),
          )
      )
      );
      return;
    }
    pageController.jumpToPage(answers.length);
  }
}
