import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizzy/core/core_components/app_logo.dart';
import 'package:quizzy/modules/quiz/presintation/controller/sections_cubit.dart';
import 'package:quizzy/modules/quiz/presintation/controller/sections_cubit.dart';
import 'package:quizzy/modules/quiz/presintation/screens/section_screen.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/consts/assets_const.dart';
import '../../../../core/core_components/status_component.dart';
import '../../../profile/presintation/screens/profile_screen.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static const name = '/home';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const AppLogo(),
        actions: [
          Container(
            margin: EdgeInsets.all(2.w),
            decoration: BoxDecoration(
              border: Border.all(),
              borderRadius: BorderRadius.circular(2.w)
            ),
            child: IconButton(
                onPressed: (){
                  Navigator.pushNamed(context, ProfileScreen.name);
                },
                icon: Icon(Icons.person_outline)
            ),
          )
        ],
      ),
      body: Container(
        width: double.infinity,
        padding: EdgeInsets.all(5.w),
        child: Column(
          children: [
            SizedBox(
              height: 10.w,
              child: const FittedBox(
                child: Text(
                  'Choose Your Quiz Now',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ),
            SizedBox(
              height: 60.w,
              width: 60.w,
              child: Image.asset(
                ImagesConst.choose,
                fit: BoxFit.fitWidth,
              ),
            ),
            Expanded(
                child: BlocBuilder<SectionsCubit, SectionsState>(
                  builder: (context, state) {
                    return StatusComponent(
                      status: state.dataState.status,
                      onSuccess: (context) => ListView.builder(
                        itemCount: state.dataState.data!.length,
                        itemBuilder: (context,i) => GestureDetector(
                          onTap: ()=> Navigator.push(context,
                            MaterialPageRoute(builder: (context) => SectionsScreen(state.dataState.data![i])),
                          ),
                          child: Container(
                            width: double.infinity,
                            height: 15.w,
                            margin: EdgeInsets.all(5.w).copyWith(bottom: 1.w),
                            decoration: BoxDecoration(
                              color: Color(0xFF3CCFCF),
                              borderRadius: BorderRadius.circular(2.5.w),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              state.dataState.data![i].name,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.sp,
                                fontWeight: FontWeight.w700
                              ),
                            ),
                          ),
                        ),
                      ),
                      onError: (context) => Center(
                        child: Text(state.dataState.message),
                      ),
                    );
                  },
                )
            )
          ],
        ),
      ),
    );
  }
}
