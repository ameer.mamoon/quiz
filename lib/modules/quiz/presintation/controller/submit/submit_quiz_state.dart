part of 'submit_quiz_cubit.dart';


@immutable
abstract class SubmitQuizState {
  final DataState<bool> dataState;

  const SubmitQuizState(this.dataState);
}

class SubmitQuizInitialState extends SubmitQuizState {
  const SubmitQuizInitialState():super(const DataState());
}

class SubmitQuizLoadingState extends SubmitQuizState {
  const SubmitQuizLoadingState():super(const DataState(status: DataStatus.loading));
}

class SubmitQuizResultState extends SubmitQuizState {
  const SubmitQuizResultState(super.dataState);
}
