import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizzy/modules/quiz/data/data_source/quizzes_data_source.dart';

import '../../../../../core/data_state/data_state.dart';
import '../../../../../core/handler/handler.dart';
import '../../../data/models/section_model.dart';

part 'submit_quiz_state.dart';

class SubmitQuizCubit extends Cubit<SubmitQuizState> {
  SubmitQuizCubit() : super(const SubmitQuizInitialState());

  void submit(QuizModel quiz,int correct)async{
    emit(const SubmitQuizLoadingState());
    var result = await handle<bool>(() => QuizzesDataSource.submitQuiz(quiz, correct));
    emit(SubmitQuizResultState(result));
  }
}
