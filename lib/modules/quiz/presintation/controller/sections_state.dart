part of 'sections_cubit.dart';


@immutable
abstract class SectionsState {
  final DataState<List<SectionModel>> dataState;

  const SectionsState(this.dataState);
}

class SectionsInitialState extends SectionsState {
  const SectionsInitialState():super(const DataState());
}

class SectionsLoadingState extends SectionsState {
  const SectionsLoadingState():super(const DataState(status: DataStatus.loading));
}

class SectionsResultState extends SectionsState {
  const SectionsResultState(super.dataState);
}
