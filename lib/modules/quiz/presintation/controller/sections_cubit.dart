import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:quizzy/modules/quiz/data/models/section_model.dart';

import '../../../../core/data_state/data_state.dart';
import '../../../../core/handler/handler.dart';
import '../../data/data_source/quizzes_data_source.dart';

part 'sections_state.dart';

class SectionsCubit extends Cubit<SectionsState> {
  SectionsCubit() : super(const SectionsInitialState());

  void load()async{
    emit(const SectionsLoadingState());
    var result = await handle<List<SectionModel>>(() => QuizzesDataSource.getSections());
    emit(SectionsResultState(result));
  }
}
