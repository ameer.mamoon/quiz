import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:quizzy/core/storage/storage_handler.dart';
import 'package:sizer/sizer.dart';

import 'modules/auth/presintation/controller/login/login_cubit.dart';
import 'modules/auth/presintation/controller/register/register_cubit.dart';
import 'modules/auth/presintation/controller/splash/splash_cubit.dart';
import 'modules/auth/presintation/screens/login_screen.dart';
import 'modules/auth/presintation/screens/register_screen.dart';
import 'modules/auth/presintation/screens/splash_screen.dart';
import 'modules/profile/presintation/controller/profile_cubit.dart';
import 'modules/profile/presintation/screens/profile_screen.dart';
import 'modules/quiz/presintation/controller/sections_cubit.dart';
import 'modules/quiz/presintation/screens/home_screen.dart';

void main() async {
  await StorageHandler.init();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
        apiKey: 'AIzaSyB5R88DG4ZBOXdzS6ZtbeFkIesnzqbM9fc',
        appId: '1:954033504372:android:bd9c8d4f898fe28f7b1121',
        messagingSenderId: '954033504372',
        projectId: 'quizz-2a827'),
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Sizer(
        builder: (_, __, ___) => MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                primaryColor: Colors.red,
                appBarTheme: const AppBarTheme(
                  color: Color(0xFFF0F0F0),
                  elevation: 0,
                  centerTitle: true,
                  foregroundColor: Colors.black,
                  shape: RoundedRectangleBorder(
                    side: BorderSide()
                  )
                ),
                scaffoldBackgroundColor: Colors.white
              ),
              routes: {

                SplashScreen.name: (_) => BlocProvider(
                      create: (_) => SplashCubit()..splash(),
                      child: const SplashScreen()
                ),

                LoginScreen.name: (_) => BlocProvider(
                    create: (_)=> LoginCubit(),
                    child: LoginScreen()
                ),

                RegisterScreen.name: (_) => BlocProvider(
                    create: (_) => RegisterCubit(),
                    child: RegisterScreen()
                ),

                HomeScreen.name:(_) => BlocProvider(
                    create: (_) => SectionsCubit()..load(),
                    child: const HomeScreen()
                ),

                ProfileScreen.name:(_)=>BlocProvider(
                    create: (_) => ProfileCubit()..load(),
                    child: const ProfileScreen()
                )
              },
            )
    );
  }
}
