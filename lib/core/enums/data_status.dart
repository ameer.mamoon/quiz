part of '../data_state/data_state.dart';

enum DataStatus{
  init,
  loading,
  error,
  success
}