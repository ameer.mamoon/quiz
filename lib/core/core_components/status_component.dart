import 'package:flutter/material.dart';

import '../data_state/data_state.dart';

class StatusComponent extends StatelessWidget {

  final DataStatus status;
  final WidgetBuilder onSuccess;
  final WidgetBuilder? onInit;
  final WidgetBuilder onError;

  const StatusComponent({
    Key? key,
    required this.status,
    required this.onSuccess,
    this.onInit,
    required this.onError
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var loaderWidget = const Center(
      child: CircularProgressIndicator(),
    );
    switch(status){
      case DataStatus.error:
        return onError(context);
      case DataStatus.success:
        return onSuccess(context);
      case DataStatus.init:
        return onInit !=null ? onInit!(context) : loaderWidget;
      case DataStatus.loading:
        return loaderWidget;
    }
  }
}
