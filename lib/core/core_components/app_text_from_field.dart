import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';


class AppTextFormField extends StatefulWidget {
  final TextEditingController? controller;
  final String? hint;
  final Widget? icon;
  final bool isPass;
  final TextInputType? keyboardType;
  final FormFieldValidator<String>? validator;
  final BorderRadius? borderRadius;

  const AppTextFormField({
    super.key,
    this.controller,
    this.hint,
    this.icon,
    this.keyboardType,
    this.isPass = false,
    this.validator,
    this.borderRadius ,
  });

  @override
  // ignore: library_private_types_in_public_api
  _AppTextFormFieldState createState() => _AppTextFormFieldState();
}

class _AppTextFormFieldState extends State<AppTextFormField> {
  bool _hide = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.w , vertical: 1.5.h),
      child: TextFormField(
        controller: widget.controller,
        validator: widget.validator,
        decoration: InputDecoration(
            hintText: widget.hint,
            label: widget.hint != null ? Text(widget.hint!): null,
            prefixIcon: widget.icon,
            hintStyle: TextStyle(
              color: Colors.grey[400]
            ),
            border: OutlineInputBorder(
              borderRadius: widget.borderRadius ??  BorderRadius.zero,
              borderSide: BorderSide(
                color: Colors.grey[400]!
              )
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderRadius: widget.borderRadius ??  BorderRadius.zero,
            ),
            suffixIcon: widget.isPass
                ? IconButton(
                    onPressed: () {
                      setState(() {
                        _hide = !_hide;
                      });
                    },
                    icon: _hide
                        ? const Icon(Icons.visibility_off)
                        : const Icon(Icons.visibility))
                : null,
          isDense: true
        ),
        obscureText: widget.isPass && _hide,
        keyboardType: widget.keyboardType,
      ),
    );
  }
}
