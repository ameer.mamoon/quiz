import 'package:flutter/material.dart';
import 'package:quizzy/core/consts/assets_const.dart';
import 'package:sizer/sizer.dart';

class AppLogo extends StatelessWidget {
  const AppLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'logo',
      child: Image.asset(
          ImagesConst.logo,
          fit: BoxFit.fitWidth,
      ),
    );
  }
}
