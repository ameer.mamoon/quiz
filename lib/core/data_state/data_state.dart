

part '../enums/data_status.dart';

class DataState<T>{

  final DataStatus status;
  final T? data;
  final String message;

  const DataState({this.status = DataStatus.init, this.data, this.message = ''});

  @override
  String toString()
    => 'DataState<${T.runtimeType}>(status : $status , data : $data , message : $message)';

  factory DataState.error(String message) => DataState(
    status: DataStatus.error,
    message: message
  );

  factory DataState.data(T data,{String message = ''}) => DataState(
      status: DataStatus.success,
      data: data,
      message: message
  );

}