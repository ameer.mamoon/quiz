
class ImagesConst {
  static const _base = 'assets/images';

  static const logo = '$_base/logo.png';
  static const login = '$_base/login.png';
  static const choose = '$_base/choose.png';
  static const quiz = '$_base/qize.png';

}