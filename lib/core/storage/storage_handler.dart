
import 'package:get_storage/get_storage.dart';

class StorageHandler {

  static StorageHandler? _handler;
  StorageHandler._();
  factory StorageHandler() => _handler ??= StorageHandler._();

  static Future<void> init() =>
      GetStorage.init();

  final _storage = GetStorage();

  String get email => _storage.read('email');

  Future<void> setEmail(String email) => _storage.write('email', email);

  bool get hasEmail => _storage.hasData('email');

  String get password => _storage.read('password');

  Future<void> setPassword(String password) => _storage.write('password', password);

  bool get hasPassword => _storage.hasData('password');

  Future<void> removeData()async{
    await _storage.remove('email');
    await _storage.remove('password');
  }


}